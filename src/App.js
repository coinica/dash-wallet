import React from "react";
import { Dashboard, Login, Transaction, Send, Receive } from "./pages";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./styles/App.css";
import { ProtectedRoute } from "./helpers/routes";

export default function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/login" component={Login} />
        <ProtectedRoute exact path="/" component={Dashboard} />
        <ProtectedRoute exact path="/transaction" component={Transaction} />
        <ProtectedRoute exact path="/send" component={Send} />
        <ProtectedRoute exact path="/receive" component={Receive} />
      </Switch>
    </Router>
  );
}
