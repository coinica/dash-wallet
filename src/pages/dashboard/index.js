import React from "react";
import { Dlayout } from "../../components";
import { Row, Col, Card } from "antd";
import "./styles/dashboard.css";

export default function Dashboard() {
  const card = {
    background: "#181C35",
    border: "none",
    borderRadius: "8px",
    color: "#fff",
    width: "100%",
    height: "100%",
  };
  return (
    <Dlayout>
      <Row gutter={16}>
        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
          <Card style={card} className="dashboard__totalBalance">
            <p>Dash Balance</p>
            <span>{localStorage.getItem("totalBalance")}</span>
          </Card>
        </Col>
        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
          <Card style={card} className="dashboard__totalBalance">
            <p>Total USD</p>
            <span>3000</span>
          </Card>
        </Col>
        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
          <Card style={card} className="dashboard__totalBalance">
            <p>Your Address</p>
            <span>{localStorage.getItem("addresses")}</span>
          </Card>
        </Col>
      </Row>
    </Dlayout>
  );
}
