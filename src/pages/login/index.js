import React, { useState } from "react";
import { CreateHDWallet, RestoreHDWallet } from "../../components";
import { Row, Col, Button } from "antd";
import "./styles/login.css";

export default function Login() {
  const [isRestore, setIsRestore] = useState(true);
  const toggleRestore = () => setIsRestore(true);
  const toggleCreate = () => setIsRestore(false);

  return (
    <div className="login">
      <Row className="login__container">
        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
          <Row justify="end" className="login__header">
            <div
              className={
                isRestore ? "login__headerSignup" : "login__headerLogin"
              }
            >
              <Button type="text" onClick={toggleRestore}>
                Restore Account
              </Button>
            </div>
            <div
              className={
                isRestore ? "login__headerLogin" : "login__headerSignup"
              }
            >
              <Button type="text" onClick={toggleCreate}>
                Create Account
              </Button>
            </div>
          </Row>
          <Row align="middle" className="login__formField">
            <div style={{ width: "100%" }}>
              {isRestore ? <RestoreHDWallet /> : <CreateHDWallet />}
            </div>
          </Row>
        </Col>
      </Row>
    </div>
  );
}
