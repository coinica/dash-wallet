import React, { useState } from "react";
import { Form, Input, Button } from "antd";
// import { useHistory } from "react-router-dom";
import "./styles/signupEmail.css";
// import { Mnemonic } from "@dashevo/dashcore-lib";

export default function RestoreHDWallet() {
  const [loading, setLoading] = useState(false);
  // const history = useHistory();

  // const createWallet = (newSeed, password) => {
  //   var address = newSeed
  //     .toHDPrivateKey()
  //     .derive("m/44'/5'/0'/0/0")
  //     .privateKey.toAddress()
  //     .toString();
  //   var encryptedHdSeed = this.encrypt(newSeed.toString(), password);
  //   var encryptedPasswordHash = this.getEncryptedPasswordHash(password);
  //   this.setState({
  //     ledger: undefined,
  //     trezor: undefined,
  //     hdSeedE: encryptedHdSeed,
  //     encryptedPasswordHash: encryptedPasswordHash,
  //     totalBalance: 0,
  //     addresses: [address],
  //     loading: false,
  //     mode: "",
  //     rememberPassword: password,
  //   });
  //   localStorage.setItem("lastLoginTime", new Date().getTime().toString());
  //   localStorage.setItem("encryptedPasswordHash", encryptedPasswordHash);
  //   localStorage.setItem("hdSeedE", encryptedHdSeed);
  //   localStorage.setItem("totalBalance", 0);
  //   localStorage.setItem("addresses", address);
  // };

  const handleSignup = (val) => {
    setLoading(true);
    // AxiosInstance()
    //   .post("/registerbyphone", {
    //     email: val.email,
    //     password: val.password,
    //   })
    //   .then((res) => {
    //     if (res.data.message === "Successfully registered!") {
    //       message.success(res.data.message);
    //       Cookie.set("phone", val.phone);
    //       history.push("/verifyphone");
    //     } else {
    //       message.error(res.data.message);
    //     }
    //   });
    setLoading(false);
  };

  return (
    <div className="signupEmail">
      <Form onFinish={handleSignup}>
        <Form.Item
          name="seed"
          rules={[{ required: true, message: "Please input your Seed Phrase" }]}
        >
          <Input
            className="signupEmail__emailField"
            placeholder="Seed Phrase"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Please input your Password!" }]}
          hasFeedback
        >
          <Input.Password
            className="signupEmail__passwordField"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item
          name="confirm_password"
          rules={[
            {
              required: true,
              message: "Please confirm your password!",
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  "The two passwords that you entered do not match!"
                );
              },
            }),
          ]}
          hasFeedback
        >
          <Input.Password
            className="signupEmail__passwordField"
            placeholder="Confirm Password"
          />
        </Form.Item>
        <Form.Item>
          <div className="signupEmail__btnSignup">
            <Button htmlType="submit" loading={loading}>
              Sign Up
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
}
