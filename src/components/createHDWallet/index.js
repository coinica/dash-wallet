import React, { useState } from "react";
import { Form, Input, Button, message } from "antd";
import { useHistory } from "react-router-dom";
import "./styles/createHDWallet.css";
import CryptoJS from "crypto-js";
import { Mnemonic } from "@dashevo/dashcore-lib";

export default function CreateHDWallet() {
  const [loading, setLoading] = useState(false);
  const [seed] = useState(new Mnemonic().toString());
  const history = useHistory();

  const handleSignup = (val) => {
    setLoading(true);
    // AxiosInstance()
    //   .post("/registerbyphone", {
    //     phone: "+855" + val.phone.replace(/^0+/, ""),
    //     password: val.password,
    //   })
    //   .then((res) => {
    //     if (res.data.message === "Successfully registered!") {
    //       message.success(res.data.message);
    //       Cookie.set("phone", val.phone);
    //       history.push("/verifyphone");
    //     } else {
    //       message.error(res.data.message);
    //     }
    //   });
    createWallet(new Mnemonic(seed), val.password);
    setLoading(false);
  };

  const createWallet = (newSeed, password) => {
    try {
      var address = newSeed
        .toHDPrivateKey()
        .derive("m/44'/5'/0'/0/0")
        .privateKey.toAddress()
        .toString();
      console.log(address);
      var encryptedHdSeed = encrypt(newSeed.toString(), password);
      var encryptedPasswordHash = getEncryptedPasswordHash(password);
      localStorage.setItem("lastLoginTime", new Date().getTime().toString());
      localStorage.setItem("encryptedPasswordHash", encryptedPasswordHash);
      localStorage.setItem("hdSeedE", encryptedHdSeed);
      localStorage.setItem("totalBalance", 0);
      localStorage.setItem("addresses", address);
      message.success("Successfully created hd wallet!");
      history.push("/");
    } catch {
      message.error("Error in creating hd wallet!");
    }
  };

  const encrypt = (text, key) => {
    try {
      var iv = CryptoJS.enc.Hex.parse("F2EBAE2CDF804895B5C091D0310169C9");
      var cfg = {
        mode: CryptoJS.mode.CBC,
        iv: iv,
        padding: CryptoJS.pad.Pkcs7,
      };
      var ciphertext = CryptoJS.AES.encrypt(text, key, cfg);
      return ciphertext.toString();
    } catch (err) {
      console.log(err);
      return "";
    }
  };
  // const decrypt = (encryptedData, key) => {
  //   try {
  //     var iv = CryptoJS.enc.Hex.parse("F2EBAE2CDF804895B5C091D0310169C9");
  //     var cfg = {
  //       mode: CryptoJS.mode.CBC,
  //       iv: iv,
  //       padding: CryptoJS.pad.Pkcs7,
  //     };
  //     var decrypted = CryptoJS.AES.decrypt(encryptedData, key, cfg);
  //     return decrypted.toString(CryptoJS.enc.Utf8);
  //   } catch (err) {
  //     console.log(err);
  //     return "";
  //   }
  // };

  const getEncryptedPasswordHash = (password) => {
    // The password is never stored, we derive it and only check if the hash is equal
    var passwordHash = deriveHash(password);
    return encrypt(passwordHash, "9ADE0896B2594184BA36E757C8E6EFD7");
  };
  const deriveHash = (password) => {
    return CryptoJS.SHA256(password).toString();
  };

  return (
    <div className="createhdwallet">
      <span>
        Locally generated HD Wallet Seed, Before creating account, Save it!
      </span>
      <p className="createhdwallet__phoneField">{seed}</p>
      <Form onFinish={handleSignup}>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Please input your Password!" }]}
          hasFeedback
        >
          <Input.Password
            className="createhdwallet__passwordField"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item
          name="confirm_password"
          rules={[
            {
              required: true,
              message: "Please confirm your password!",
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  "The two passwords that you entered do not match!"
                );
              },
            }),
          ]}
          hasFeedback
        >
          <Input.Password
            className="createhdwallet__passwordField"
            placeholder="Confirm Password"
          />
        </Form.Item>
        <Form.Item>
          <div className="createhdwallet__btnSignup">
            <Button htmlType="submit" loading={loading}>
              Create
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
}
