export default function IsAuthenticated() {
  const encryptedPasswordHash = localStorage.getItem("encryptedPasswordHash");
  const hdSeedE = localStorage.getItem("hdSeedE");
  if (encryptedPasswordHash && hdSeedE) return true;
  else return false;
}
